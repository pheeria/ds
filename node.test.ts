import { assertEquals, assertStrictEquals } from "https://deno.land/std/testing/asserts.ts";
import { Node } from "./node.ts";

Deno.test("construct", () => {
    let types = [
        "different",
        69,
        true,
        3.14,
        { ob: "ject" }
    ];

    types.forEach(t => {
        let node = new Node(t);
        assertEquals(node.data, t);
        assertEquals(node.next, undefined);
    });
});

Deno.test("link", () => {
    let first = new Node({
        country: "USA"
    });
    let second = new Node({
        country: "Netherlands"
    });

    first.link(second);
    assertStrictEquals(first.next, second);
});

Deno.test("unlink", () => {
    let node = new Node(433);
    let linked = new Node(442);
    node.link(linked);
    assertEquals(node.next, linked);

    node.unlink();
    assertEquals(node.next, undefined);
});
