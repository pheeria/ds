import { assertEquals } from "https://deno.land/std/testing/asserts.ts";
import { SinglyLinkedList } from "./singly-linked.ts";

Deno.test("append items", () => {
    let list = new SinglyLinkedList<number>();

    let numbers = [42, 34, 27];
    numbers.forEach(n => list.append(n));

    assertEquals(list.length, numbers.length);
    numbers.forEach((n, i) => assertEquals(list.at(i), n));
});

Deno.test("deleting from an empty list doesn't fail", () => {
    let list = new SinglyLinkedList<number>();

    list.delete();

    assertEquals(list.length, 0);
});

Deno.test("delete root", () => {
    let list = new SinglyLinkedList<number>();

    list.append(42);
    list.delete();

    assertEquals(list.length, 0);
});

Deno.test("delete an item", () => {
    let list = new SinglyLinkedList<number>();

    let numbers = [42, 34];
    numbers.map(n => list.append(n));
    list.delete();

    assertEquals(list.length, numbers.length - 1);
});

Deno.test("array comparison", () => {
    let list = new SinglyLinkedList<string>();

    list.delete()
        .append("delete")
        .delete()
        .append("1")
        .append("delete")
        .delete()
        .append("2")
        .append("delete")
        .delete()
        .append("3");

    assertEquals(list.toArray(), ["1", "2", "3"]);
});
