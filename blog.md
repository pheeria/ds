# An ode to node

Whenever data structures are discussed, usually the talk is about linked lists.
Or maybe about the trees, and [how obsessed a lot of top companies are to ask
candidates about them][hackerrank]. 

So, as a humble impostor syndrome fighter, I decided to start yet another side project,
which is unlikely to be finished. It shall be a series on as many data structures,
as I can nicely describe until I get bored. 

As an implementation language I'm picking TypeScript. On one hand, it is a typed language,
which in my opinion is a nice addition while trying to express data structures, on the other hand,
being a C-like language, it has a familiar syntax. To avoid all of the [JavaScript Fatigue][fatigue]
I decided to try out Deno - no `node_modules/`, included TS support and a test framework.

This is also a case for practicing TDD. 
Even though editors and IDEs don't like [an approach of accessing undefined functions][vs]

For this small project of mine I'll be using OOP as a paradigm, not because I feel like it's
better, but rather because Abstract Data Types can be nicely described as objects and 
with one simple trick, provide method chaining.

Let's start with a Node. I think it is underrated because of its flexibility. Nodes
are used as fundamental building block of various data structures, some of which we will
try to cover.

```sh
# Install Deno
brew install deno
# Install Deno assertions for running tests
deno cache https://deno.land/std/testing/asserts.ts
```




The same way bits and bytes are the basic building blocks of primitive data types, the nodes
are the same basics for abstract data types - lists, stacks, queues (all of which are actually
list subtypes), trees etc. When I first tried implementing a list, it was an Array List.
This approach makes more sense for the languages with a stricter array type.

[hackerrank]: https://blog.hackerrank.com/the-unhealthy-obsession-with-tree-questions/
[fatigue]: https://hackernoon.com/how-it-feels-to-learn-javascript-in-2016-d3a717dd577f
[vs]: https://charlespetzold.com/etc/DoesVisualStudioRotTheMind.html

[shlemiel]: https://www.joelonsoftware.com/2001/12/11/back-to-basics/
[taste]: https://medium.com/@bartobri/applying-the-linus-tarvolds-good-taste-coding-requirement-99749f37684a
