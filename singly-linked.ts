import { Node } from "./node.ts";

export class SinglyLinkedList<T> {
    root?: Node<T>;
    #length: number;

    constructor() {
        this.#length = 0;
    }

    get length(): number {
        return this.#length;
    }

    at(index: number): T {
        if (index < 0 || index >= this.#length || !this.root) {
            throw new Error("You're wrong!");
        } else {
            let result = this.root.data;
            let current = this.root;

            for (let i = 0; i < index && current.next; i++) {
                current = current.next;
                result = current.data;
            }

            return result;
        }
    }
    append(data: T) {
        let node = new Node(data);

        if (this.root) {
            let current = this.root;

            while (current.next) {
                current = current.next;
            }

            current.link(node);
        } else {
            this.root = node;
        }
        this.#length++;
        return this;
    }
    delete() {
        if (this.root) {
            let current = this.root;

            if (current.next) {
                while (current.next && current.next.next) {
                    current = current.next;
                }

                current.unlink();
            } else {
                this.root = undefined;
            }
            this.#length--;
        }

        return this;
    }

    toString(): string {
        return JSON.stringify(this, null, 4);
    }
    toArray(): T[] {
        let result: T[] = [];

        if (this.root) {
            let current = this.root;
            result.push(current.data);

            while (current.next) {
                current = current.next;
                result.push(current.data);
            }
        }

        return result;
    }
}
