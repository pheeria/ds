export class Node<T> {
    data: T;
    next?: Node<T>;

    constructor(data: T) {
        this.data = data;
    }
    
    link(node: Node<T>) {
        this.next = node;
    }

    unlink() {
        this.next = undefined;
    }
}
