# DS

## Installation
```sh
# Install Deno
brew install deno
# Install Deno assertions for running tests
deno cache https://deno.land/std/testing/asserts.ts
```

## Running Tests
```sh
deno test
```
